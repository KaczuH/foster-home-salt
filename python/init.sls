{% set python_version = salt['pillar.get']('python:lookup:version') %}
{% set project_name = salt['pillar.get']('project_name', 'default_project') %}

django_group:
  group:
    - name: django_group
    - present

{{ project_name }}_django:
  user.present:
    - groups:
      - django_group
    - require:
      - group: django_group

pyenv-deps:
  pkg.installed:
    - pkgs:
      - make
      - build-essential
      - libssl-dev
      - zlib1g-dev
      - libbz2-dev
      - libreadline-dev
      - libsqlite3-dev
      - wget
      - curl
      - llvm

python-{{ python_version }}:
  pyenv.installed:
    - require:
      - pkg: pyenv-deps

common-pkgs:
  pkg.installed:
    - names:
      - git

bitbucket_repo:
  ssh_known_hosts:
    - present
    - user: root
    - enc: rsa
    - fingerprint: 97:8c:1b:f2:6f:14:6b:5c:3b:ec:aa:46:46:74:7c:40
    - name: bitbucket.com


{{ project_name }}_app:
  git.latest:
    - rev: development
    - force_checkout: True
    - name: {{ salt['pillar.get']('python:repository_address') }}
    - target: {{ salt['pillar.get']('project_root_path') }}
    - identity: /root/.ssh/{{ project_name }}_id_rsa
    - require:
      - pkg: git
      - ssh_known_hosts: bitbucket_repo

{{ project_name }}_ownership:
  file:
    - directory
    - name: {{ salt['pillar.get']('project_root_path') }}
    - user: www-data
    - group: adm
    - recurse:
      - user
      - group

{{ project_name }}_django_log:
  file:
    - directory
    - name: /var/log/django
    - user: www-data
    - group: adm
    - recurse:
      - user
      - group

{{ project_name }}_venv:
  virtualenv.managed:
    - name: {{ salt['pillar.get']('python:virtualenv_path') }}
    - pip: True
    - venv_bin: /usr/local/pyenv/versions/{{ python_version }}/bin/pyenv
    - requirements: {{ salt['pillar.get']('python:requirements_path') }}
