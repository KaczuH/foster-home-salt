{% from "postgres/map.jinja" import postgres with context %}
{% set project_name = salt['pillar.get']('project_name', 'default_project') %}

postgresql-pkgs:
  pkg.installed:
    - names:
      - libpq-dev
      - python-dev
      - postgresql
      - postgresql-contrib


run-postgresql:
  service.running:
    - enable: true
    - name: {{ postgres.service }}
    - require:
      - pkg: postgresql-pkgs

{{ project_name }}_postgres:
  postgres_user.present:
    - name: {{ postgres.db_role }}
    - createdb: False
    - password: {{ project_name }}_postgres
    - require:
      - service: {{ postgres.service }}

{{ project_name }}_db:
  postgres_database.present:
    - name: {{ postgres.db_name }}
    - encoding: 'UTF8'
    - lc_ctype: 'en_US.UTF-8'
    - lc_collate: 'en_US.UTF-8'
    - owner: {{ postgres.db_role }}
    - require:
        - postgres_user: {{ postgres.db_role }}
