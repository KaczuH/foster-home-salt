{% set project_name = salt['pillar.get']('project_name', 'default_project') %}
{% set uwsgi_root_path = salt['pillar.get']('uwsgi:root_path', 'uwsgi_root_path') %}
{% set uwsgi_log_path = salt['pillar.get']('uwsgi:log_path', 'uwsgi_log_path') %}

include:
  - uwsgi

{{ uwsgi_log_path }}:
  file.directory:
    - user: www-data
    - group: adm
    - mode: 755
    - makedirs: True

{{ uwsgi_log_path }}/{{ project_name }}.log:
  file.managed:
    - user: www-data
    - group: adm
    - mode: 755
    - makedirs: True

uwsgiini:
  file.managed:
    - name: {{ salt['pillar.get']('uwsgi:root_path', 'root_path') }}/vassals/{{ project_name }}_uwsgi.ini
    - source: salt://uwsgi/templates/{{ project_name }}_uwsgi.jinja
    - template: jinja
    - makedirs: True
    - mode: 755


uwsgi_emperorini:
  file.managed:
    - name: {{ salt['pillar.get']('uwsgi:root_path', 'root_path') }}/emperor/emperor.ini
    - source: salt://uwsgi/templates/emperor.ini.jinja
    - template: jinja
    - makedirs: True
    - mode: 755

uwsgi_emperorconf:
  file.managed:
    - name: /etc/init/emperor.conf
    - source: salt://uwsgi/templates/emperor.conf.jinja
    - template: jinja
    - makedirs: True
    - mode: 755


uwsgi_emperor:
  service.running:
    - name: emperor
    - enable: True
    - watch:
      - file : uwsgi_emperorini