include:
  - common

postgres:
    lookup:
        service: postgresql
        version: 9.3
        db_role: foster_home_postgres
        db_name: foster_home_db
