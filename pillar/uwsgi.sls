include:
    - common

uwsgi:
    # TODO (kniski) bug rendering this var key is not found (render ordering?)
    #bin_path: {{ salt['pillar.get']('python:virtualenv_path') }}/bin/uwsgi
    bin_path: /var/www/{{ salt['pillar.get']('project_name', 'default_project') }}/bin/uwsgi
    root_path: /etc/uwsgi
    log_path: /var/log/uwsgi

appini:
    processes: 4