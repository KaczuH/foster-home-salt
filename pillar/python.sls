include:
  - common

python:
  requirements_path: /opt/foster_home_django/requirements/development.txt
  repository_address: git@bitbucket.org:KaczuH/foster-home.git
  virtualenv_path: /var/www/{{ salt['pillar.get']('project_name', 'default_project') }}
  lookup:
    version: 3.3.6
