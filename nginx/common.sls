{% from "nginx/map.jinja" import nginx as nginx_map with context %}

nginx_group:
  group.present:
    - name: {{ nginx_map.default_group }}

nginx_user:
  file.directory:
    - name: {{ nginx_map.home }}
    - user: {{ nginx_map.default_user }}
    - group: {{ nginx_map.default_group }}
    - mode: 0755
    - require:
      - user: nginx_user
      - group: nginx_group
  user.present:
    - name: {{ nginx_map.default_user }}
    - home: {{ nginx_map.home }}
    - groups:
      - {{ nginx_map.default_group }}
    - require:
      - group: nginx_group