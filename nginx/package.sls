{% from "nginx/map.jinja" import nginx with context %}
{% set project_name = salt['pillar.get']('project_name', 'default_project') %}

nginx:
  pkg.installed:
    - name: {{ nginx.package }}
  service.running:
    - enable: True
    - restart: True
    - name: nginx
    - require:
      - pkg: {{ nginx.package }}

nginx_conf:
  file.managed:
    - name: /etc/nginx/sites-enabled/{{ project_name }}.conf
    - source: salt://nginx/templates/{{ project_name }}.jinja
    - template: jinja
    - makedirs: True
    - mode: 755